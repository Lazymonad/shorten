const express = require("express");
const glob = require("glob");
const bodyParser = require("body-parser");
const expressVue = require("express-vue");
const logger = require("morgan");

module.exports.init = (app, config) => {
    const router = express.Router();
    let logType = "dev";
    app.locals.rootPath = config.root;

    const vueOptions = {};
    const expressVueMiddleware = expressVue.init(vueOptions);
    app.use(expressVueMiddleware);

    app.use(bodyParser.json());
    app.use(
        bodyParser.urlencoded({
            extended: true,
        })
    );

    app.use(app.locals.rootPath, express.static(config.root));

    // Logging
    app.use(logger(logType));

    app.use("/", router);

    let controllers = glob.sync(config.root + "/../routes/**/*.js");
    controllers.forEach(function (controller) {
        module.require(controller)(router);
    });

    function error404handler(req, res) {
        const data = {
            title: "Error 404",
        };
        req.vueOptions = {
            head: {
                title: "Error 404",
            },
        };
        res.statusCode = 404;
        res.renderVue("./app/routes/error.vue", data, req.vueOptions);
    }
    app.use(error404handler);

    function genericErrorHandler(error, req, res, next) {
        // Slack integration
        res.statusCode = 500;
        let data = {
            debug: true,
            errorCode: error.code,
            error: error.stack,
        };
        if (res.statusCode) {
            res.renderVue("error.vue", data);
        } else {
            next();
        }
    }
    app.use(genericErrorHandler);
};
