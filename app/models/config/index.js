const path = require("path");

class Config {
    constructor() {
        this.env = "dev";
        this.root = path.normalize(__dirname + "/..");
        this.rootPath = "/";
        this.app = {
            name: "URI Shorter",
        };
        this.port = 8000;
    }
}
module.exports = Config;
