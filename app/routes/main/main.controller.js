module.exports = (router) => {
    router.get("/", (req, res) => {
        const data = {};
        req.vueOptions.head.styles.push({
            src:
                "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css",
        });
        req.vueOptions.head.scripts.push({
            src:
                "https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js",
        });
        req.vueOptions.head.scripts.push({
            src: "https://unpkg.com/axios/dist/axios.min.js",
        });
        req.vueOptions.head.title = "URI Shorter";
        res.renderVue("app/routes/main/main.vue", data, req.vueOptions);
    });
};
